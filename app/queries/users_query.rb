class UsersQuery
  attr_accessor :initial_scope

  def initialize(initial_scope = User.all)
    @initial_scope = initial_scope
  end

  def call
    return @initial_scope
  end

  def filter_by_first_name(first_name)
    @initial_scope = @initial_scope.where(first_name: first_name)
    return self
  end

  def filter_by_last_name(last_name)
    @initial_scope = @initial_scope.where(last_name: last_name)
    return self
  end

  def filter_by_score(score)
    @initial_scope = @initial_scope.where(score: score)
    return self
  end

  def filter_by_age(age)
    @initial_scope = @initial_scope.where(age: age)
    return self
  end

  def filter_by_max_score()
    @initial_scope = @initial_scope.desc(:score)
    return self
  end

  def filter_by_min_score()
    @initial_scope = @initial_scope.asc(:score)
    return self
  end

  def filter_by_max_age()
    @initial_scope = @initial_scope.desc(:age)
    return self
  end

  def filter_by_min_age()
    @initial_scope = @initial_scope.asc(:age)
    return self
  end
end