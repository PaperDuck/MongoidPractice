require 'csv'

class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  def generateDummyData
    User.collection.drop

    CSV.foreach("dummy_users.csv", :headers => true) do |row|
      User.create({first_name: row["first_name"], last_name: row["last_name"], score: row["score"], age: row["age"]})
    end

    render json: {success: "DB populated with dummy data users."}, status: :ok
  end

  def test
    response = {}

    # 1
    search = "Alien"
    response[:case1] = "Filter By First Name (#{search})"
    user = UsersQuery.new.filter_by_first_name(search).call().first 
    response[:case1_result] = user.to_json(only: [:first_name, :last_name])

    # 2
    search = "Clark"
    response[:case2] = "Filter By Last Name (#{search})"
    user = UsersQuery.new.filter_by_last_name(search).call().first 
    response[:case2_result] = user.to_json(only: [:first_name, :last_name])

    # 3
    search = 338
    response[:case3] = "Filter By Score (#{search})"
    user = UsersQuery.new.filter_by_score(search).call().first 
    response[:case3_result] = user.to_json(only: [:first_name, :score])

    # 4
    search = 27
    response[:case4] = "Filter By Age (#{search})"
    user = UsersQuery.new.filter_by_age(search).call().first 
    response[:case4_result] = user.to_json(only: [:first_name, :age])

    # 5
    response[:case5] = "Filter By Max Score"
    user = UsersQuery.new.filter_by_max_score().call().first;
    response[:case5_result] = user.to_json(only: [:first_name, :score])

    # 6
    response[:case6] = "Filter By Min Score"
    user = UsersQuery.new.filter_by_min_score().call().first;
    response[:case6_result] = user.to_json(only: [:first_name, :score])

    # 7
    response[:case7] = "Filter By Top Three Score"
    user = UsersQuery.new.filter_by_max_score().call().limit(3);
    response[:case7_result] = user.to_json(only: [:first_name, :score])

    # 8
    response[:case8] = "Filter By Bottom Three Score"
    user = UsersQuery.new.filter_by_min_score().call().limit(3);
    response[:case8_result] = user.to_json(only: [:first_name, :score])

    # 9
    response[:case9] = "Filter By Oldest"
    user = UsersQuery.new.filter_by_max_age().call().first;
    response[:case9_result] = user.to_json(only: [:first_name, :age])

    # 10
    response[:case10] = "Filter By Youngest"
    user = UsersQuery.new.filter_by_min_age().call().first;
    response[:case10_result] = user.to_json(only: [:first_name, :age])

    # 11
    response[:case11] = "Filter By Top Three Oldest"
    user = UsersQuery.new.filter_by_max_age().call().limit(3);
    response[:case11_result] = user.to_json(only: [:first_name, :age])

    # 12
    response[:case12] = "Filter By Top Three Youngest"
    user = UsersQuery.new.filter_by_min_age().call().limit(3);
    response[:case12_result] = user.to_json(only: [:first_name, :age])

    # 11
    response[:case11] = "Filter By Top Three Oldest"
    user = UsersQuery.new.filter_by_max_age().call().limit(3);
    response[:case11_result] = user.to_json(only: [:first_name, :age])

    # 12
    response[:case12] = "Filter By Top Three Youngest"
    user = UsersQuery.new.filter_by_min_age().call().limit(3);
    response[:case12_result] = user.to_json(only: [:first_name, :age])

    #13
    response[:case13] = "Filter by age older than 100"
    user = User.where(:age.gt => 100)
    response[:case13_result] = user.to_json(only: [:first_name, :age])

    #14
    response[:case14] = "Filter by age younger than 100"
    user = User.where(:age.lt => 100)
    response[:case14_result] = user.to_json(only: [:first_name, :age])

    render json: response, status: :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.fetch(:user, {})
    end
end
