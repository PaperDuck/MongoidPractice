# README

## Table of Contents
---
- [General](#general)
- [How To Run](#how-to-run)
- [Mongoid Cheatsheet](#mongoid-cheatsheet)

### General
---
Rails Project to practice Mongoid Queries and also as a place for personal reference.

### How To Run
---
Make sure that you have Docker CE installed.
Use the following command in the **Terminal** to check that you have both Docker Client and Server installed
> docker version

Once you have confirmed that they are installed, use the following command to run the project
> docker-compose up

At anytime, you can perform Ctrl+C to stop the project.

### Mongoid Cheatsheet
---

#### Filter (Mongoid::Criteria)

- ##### Get everything
    `<Model>.all`
    `db.getCollection(<Model>).find()`

- ##### By Specific Attribute(s)
    `<Model>.where(attribute: <Your Search Criteria>)`

    `<Model>.where(attribute1: <Your Search Criteria>, attribute2: <Your Search Criteria>)`

    `db.getCollection(<Model>).find({attribute: <Your Search Criteria>})`

    `db.getCollection(<Model>).find({attribute1: <Your Search Criteria>, attribute2: <Your Search Criteria>})`

- ##### Sort By Attribute Value (Ascending)
    `<Model>.asc(attribute: <Your Search Criteria>)`

    `db.getCollection(<Model>).find().sort({attribute: 1})`

- ##### Sort By Attribute Value (Descending)
    `<Model>.desc(attribute: <Your Search Criteria>)`

    `db.getCollection(<Model>).find().sort({attribute: -1})`

- ##### Query By Comparison (Greater Than, Greater Than Equal)
    `<Model>.where(:attribute.gt => <Your Search Criteria>)`

    `<Model>.where(:attribute.gte => <Your Search Criteria>)`
    
    `db.getCollection(<Model>).find({attribute: {$gt: <Your Search Criteria>}})`

    `db.getCollection(<Model>).find({attribute: {$gte: <Your Search Criteria>}})`

- ##### Query By Comparison (Greater Than, Greater Than Equal)
    `<Model>.where(:attribute.lt => <Your Search Criteria>)`

    `<Model>.where(:attribute.lte => <Your Search Criteria>)`
    
    `db.getCollection(<Model>).find({attribute: {$lt: <Your Search Criteria>}})`

    `db.getCollection(<Model>).find({attribute: {$lte: <Your Search Criteria>}})`

#### Retrieving query results

- ##### Get first object
    `<Model>.<Filter>.first`

- ##### Get last object
    `<Model>.<Filter>.last`

- ##### Get all results as an array
    `<Model>.<Filter>.to_a`

- ##### Get first *N* results
    `<Model>.<Filter>.limit(n)`

- ##### Allows an iterator function block to be applied on each result
    ``` ruby
    <Model>.<Filter>.each do |result| 
        # function body here
    end 
    ```