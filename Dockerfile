FROM ruby:2.5.0

MAINTAINER Wee Keat Teo <weekeat.teo@gmail.com>

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN bundle install

# Go back to our root app folder.
WORKDIR /usr/src/app

CMD ["rails", "s", "-b", "0.0.0.0", "-p", "3000"]